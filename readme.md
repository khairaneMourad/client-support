# Description

The main role of this application is to make the communication between our client and the client support team efficient by offering a set of endpoint to add, update, and fetch client folders and messages.

# Run the application natively:

First build and package the application into a JAR file using: 

`mvn clean package`

Second run the application using the java command:

`java -jar client-support-1.0.0-SNAPSHOT.jar`

At this stage the application should be running.

# Run it using docker

First pull the image from the docker registry using:

`docker pull khairane/client-support:1.0.0`

Second run the application using the following command:

`docker run -p 8080:8080 khairane/client-support:1.0.0`

# Use the application

To use the set of endpoints exposed by our API, we highly recommend you to use the following swagger interface which will be accessible in this URL:

`http://localhost:8080/openapi-spec.html`
