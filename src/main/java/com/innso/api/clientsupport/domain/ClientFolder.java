package com.innso.api.clientsupport.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;


@NoArgsConstructor
@Setter
@Getter
public class ClientFolder {
    private String id;
    private String clientName;
    private LocalDateTime openingDate;
    private String reference;
    private List<Message> messages;

    public List<Message> getMessages() {
        if (messages == null)
            messages = new LinkedList<>();
        return messages;
    }


    public static final class Builder {
        private String id;
        private String clientName;
        private LocalDateTime openingDate;
        private String reference;
        private List<Message> messages;

        private Builder() {
        }

        public static Builder aClientFolder() {
            return new Builder();
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withClientName(String clientName) {
            this.clientName = clientName;
            return this;
        }

        public Builder withOpeningDate(LocalDateTime openingDate) {
            this.openingDate = openingDate;
            return this;
        }

        public Builder withReference(String reference) {
            this.reference = reference;
            return this;
        }

        public Builder withMessages(List<Message> messages) {
            this.messages = messages;
            return this;
        }

        public ClientFolder build() {
            ClientFolder clientFolder = new ClientFolder();
            clientFolder.setId(id);
            clientFolder.setClientName(clientName);
            clientFolder.setOpeningDate(openingDate);
            clientFolder.setReference(reference);
            clientFolder.setMessages(messages);
            return clientFolder;
        }
    }
}
