package com.innso.api.clientsupport.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;


@NoArgsConstructor
@Setter
@Getter
public class Message {
    private String id;
    private LocalDateTime date;
    private String authorName;
    private String content;
    private Canal canal;


    public static final class MessageBuilder {
        private String id;
        private LocalDateTime date;
        private String authorName;
        private String content;
        private Canal canal;

        private MessageBuilder() {
        }

        public static MessageBuilder aMessage() {
            return new MessageBuilder();
        }

        public MessageBuilder id(String id) {
            this.id = id;
            return this;
        }

        public MessageBuilder date(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public MessageBuilder authorName(String authorName) {
            this.authorName = authorName;
            return this;
        }

        public MessageBuilder content(String content) {
            this.content = content;
            return this;
        }

        public MessageBuilder canal(Canal canal) {
            this.canal = canal;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.setId(id);
            message.setDate(date);
            message.setAuthorName(authorName);
            message.setContent(content);
            message.setCanal(canal);
            return message;
        }
    }
}


