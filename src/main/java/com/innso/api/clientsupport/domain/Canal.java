package com.innso.api.clientsupport.domain;

public enum Canal {
    MAIL,
    SMS,
    FACEBOOK,
    TWITTER;
}
