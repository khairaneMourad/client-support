package com.innso.api.clientsupport.configuration;


import com.innso.api.clientsupport.adapters.outbound.InMemoryDataBase;
import org.modelmapper.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class GlobalBeanConfig {

    @Bean
    public InMemoryDataBase inMemoryDataBase(){
        return new InMemoryDataBase();
    }

    @Bean
    public ModelMapper modelMapper(){
        final ModelMapper modelMapper = new ModelMapper();

        Provider<LocalDateTime> localDateTimeProvider = new AbstractProvider<>() {
            @Override
            public LocalDateTime get() {
                return LocalDateTime.now();
            }
        };

        Converter<String, LocalDateTime> toLocalDataTime = new AbstractConverter<>() {
            @Override
            protected LocalDateTime convert(String source) {
                if(source == null) return null;
                LocalDateTime localDate = LocalDateTime.parse(source, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                return localDate;
            }
        };


        modelMapper.createTypeMap(String.class, LocalDateTime.class);
        modelMapper.addConverter(toLocalDataTime);
        modelMapper.getTypeMap(String.class, LocalDateTime.class).setProvider(localDateTimeProvider);

        return modelMapper;
    }
}
