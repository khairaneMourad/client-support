package com.innso.api.clientsupport.service;

import com.innso.api.clientsupport.domain.ClientFolder;

import java.util.List;

public interface ClientFolderService {
    String createClientFolder(ClientFolder clientFolder);

    List<ClientFolder> fetchClientFolders();

    String attachMessageToClientFolder(String messageId, ClientFolder clientFolder);

    void updateClientFolder(String clientFolderId, ClientFolder clientFolder);

    ClientFolder findClientFolder(String clientFolderId);
}
