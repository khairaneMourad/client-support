package com.innso.api.clientsupport.service;

import com.innso.api.clientsupport.domain.Message;

import java.util.List;

public interface MessageService {
    String addMessage(Message message);

    void attachMessageToClientFolder(String clientFolderId, Message message);

    Message findMessage(String messageId);

    List<Message> fetchMessages(String authorName);
}
