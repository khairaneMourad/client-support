package com.innso.api.clientsupport.service;

import com.innso.api.clientsupport.adapters.outbound.repository.ClientFolderRepository;
import com.innso.api.clientsupport.domain.ClientFolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DefaultClientFolderService implements ClientFolderService {

    private ClientFolderRepository clientFolderRepository;


    public DefaultClientFolderService(ClientFolderRepository clientFolderRepository) {
        this.clientFolderRepository = clientFolderRepository;
    }

    @Override
    public String createClientFolder(ClientFolder clientFolder) {
        log.info("Saving new client folder for ID  {}", clientFolder.getId());
        return clientFolderRepository.createClientFolder(clientFolder);
    }

    @Override
    public List<ClientFolder> fetchClientFolders() {
        log.info("Fetching all client folders");
        return clientFolderRepository.fetchClientFolders();
    }

    @Override
    public String attachMessageToClientFolder(String messageId, ClientFolder clientFolder) {
        log.info("Attaching message with message ID: {} to new client folder", messageId);
        return clientFolderRepository.attachMessageToNewClientFolder(messageId, clientFolder);
    }

    @Override
    public void updateClientFolder(String clientFolderId, ClientFolder clientFolder) {
        log.info("Updating client folder with ID: {}", clientFolder);
        clientFolderRepository.updateClientFolder(clientFolderId, clientFolder);
    }

    @Override
    public ClientFolder findClientFolder(String clientFolderId) {
        return clientFolderRepository.findClientFolderById(clientFolderId);
    }
}
