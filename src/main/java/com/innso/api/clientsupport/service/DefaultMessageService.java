package com.innso.api.clientsupport.service;

import com.innso.api.clientsupport.adapters.outbound.repository.ClientFolderRepository;
import com.innso.api.clientsupport.adapters.outbound.repository.MessageRepository;
import com.innso.api.clientsupport.domain.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DefaultMessageService implements MessageService {

    private MessageRepository messageRepository;
    private ClientFolderRepository clientFolderRepository;

    public DefaultMessageService(MessageRepository messageRepository, ClientFolderRepository clientFolderRepository) {
        this.messageRepository = messageRepository;
        this.clientFolderRepository = clientFolderRepository;
    }

    @Override
    public String addMessage(Message message) {
        log.info("Creating message");
        messageRepository.createMessage(message);
        return message.getId();
    }

    @Override
    public void attachMessageToClientFolder(String clientFolderId, Message message) {
        log.info("Attaching new message to existing client ID: {}", clientFolderId);
        clientFolderRepository.addMessageToExistingFolder(message, clientFolderId);
    }

    @Override
    public Message findMessage(String messageId) {
        log.info("Searching message for ID: {}", messageId);
        return messageRepository.findMessageById(messageId);
    }

    @Override
    public List<Message> fetchMessages(String authorName) {
        log.info("Fetching messages for author: {}", authorName);
        return messageRepository.fetchMessagesByAuthor(authorName);
    }
}
