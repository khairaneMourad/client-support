package com.innso.api.clientsupport.adapters.outbound.repository;

import com.innso.api.clientsupport.domain.ClientFolder;
import com.innso.api.clientsupport.domain.Message;

import java.util.List;

public interface ClientFolderRepository {
    String attachMessageToNewClientFolder(String idMessage, ClientFolder clientFolder);
    String createClientFolder(ClientFolder clientFolder);
    void addMessageToExistingFolder(Message message, String clientFolderId) throws RuntimeException;
    void updateClientFolder(String clientFolderId, ClientFolder clientFolder);
    List<ClientFolder> fetchClientFolders();
    ClientFolder findClientFolderById(String clientFolderId);
}
