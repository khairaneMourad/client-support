package com.innso.api.clientsupport.adapters.inbound.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
public class ClientFolderDto {
    private String id;
    private String clientName;
    private String openingDate;
    private String reference;
    private List<MessageDto> messages;

    public List<MessageDto> getMessages() {
        if (messages == null)
            messages = new LinkedList<>();
        return messages;
    }
}
