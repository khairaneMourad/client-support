package com.innso.api.clientsupport.adapters.inbound.dto;

public enum Canal {
    MAIL,
    SMS,
    FACEBOOK,
    TWITTER;
}
