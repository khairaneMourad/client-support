package com.innso.api.clientsupport.adapters.outbound.repository;

import com.innso.api.clientsupport.adapters.outbound.InMemoryDataBase;
import com.innso.api.clientsupport.domain.Message;
import com.innso.api.clientsupport.exception.EntityNotFound;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Repository
public class InMemoryMessageRepository implements MessageRepository {

    private InMemoryDataBase inMemoryDataBase;

    public InMemoryMessageRepository(InMemoryDataBase inMemoryDataBase) {
        this.inMemoryDataBase = inMemoryDataBase;
    }

    @Override
    public String createMessage(Message message) {
        message.setId(UUID.randomUUID().toString());
        inMemoryDataBase.getMessages().add(message);
        return message.getId();
    }

    @Override
    public void updateMessage(Message newMessage) {
        final Message message = findMessageById(newMessage.getId());
        if(!message.getCanal().equals(newMessage.getCanal()))
            message.setCanal(newMessage.getCanal());

        if(!message.getContent().equals(newMessage.getContent()))
            message.setContent(newMessage.getContent());

        if(!message.getDate().equals(newMessage.getDate()))
            message.setDate(newMessage.getDate());
    }

    @Override
    public List<Message> fetchMessagesByAuthor(String authorName) {
        return inMemoryDataBase.getMessages().stream()
                .filter(msg -> msg.getAuthorName().equals(authorName))
                .collect(toList());
    }

    public Message findMessageById(String messageId) {
        return inMemoryDataBase.getMessages().stream()
                .filter(message -> message.getId().equals(messageId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFound("Message not found for message ID:" + messageId));
    }
}
