package com.innso.api.clientsupport.adapters.inbound.controller;

import com.innso.api.clientsupport.adapters.inbound.dto.MessageDto;
import com.innso.api.clientsupport.domain.Message;
import com.innso.api.clientsupport.service.MessageService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class MessageController {

    private ModelMapper modelMapper;
    private MessageService messageService;

    public MessageController(ModelMapper modelMapper, MessageService messageService) {
        this.modelMapper = modelMapper;
        this.messageService = messageService;
    }

    @PostMapping("/message/add")
    public ResponseEntity<URI> addMessage(@RequestBody MessageDto messageDto) {
        final Message message = modelMapper.map(messageDto, Message.class);
        final String messageId = messageService.addMessage(message);
        return ResponseEntity.ok(URI.create("/message/" + messageId));
    }

    @GetMapping("/message/{messageId}")
    public ResponseEntity<MessageDto> findMessage(@PathVariable String messageId) {
        final MessageDto messageDto = modelMapper.map(messageService.findMessage(messageId), MessageDto.class);
        return ResponseEntity.ok(messageDto);
    }

    @PostMapping("/message/with/clientfolder/{clientFolderId}")
    public ResponseEntity attachMessageToClientFolder(@PathVariable String clientFolderId, @RequestBody MessageDto messageDto) {
        final Message message = modelMapper.map(messageDto, Message.class);
        messageService.attachMessageToClientFolder(clientFolderId, message);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/messages/{authorName}")
    public ResponseEntity<Set<MessageDto>> fetchMessages(@PathVariable String authorName) {
        final Set<MessageDto> messages = messageService.fetchMessages(authorName).stream()
                .map(msg -> modelMapper.map(msg, MessageDto.class))
                .collect(Collectors.toSet());

        return ResponseEntity.ok(messages);
    }
}
