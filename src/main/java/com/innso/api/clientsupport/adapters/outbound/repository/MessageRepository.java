package com.innso.api.clientsupport.adapters.outbound.repository;

import com.innso.api.clientsupport.domain.Message;

import java.util.List;

public interface MessageRepository {
    String createMessage(Message message);
    void updateMessage(Message newMessage);
    List<Message> fetchMessagesByAuthor(String authorName);
    Message findMessageById(String messageId);
}
