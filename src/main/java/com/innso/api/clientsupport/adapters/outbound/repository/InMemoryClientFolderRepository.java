package com.innso.api.clientsupport.adapters.outbound.repository;

import com.innso.api.clientsupport.adapters.outbound.InMemoryDataBase;
import com.innso.api.clientsupport.domain.ClientFolder;
import com.innso.api.clientsupport.domain.Message;
import com.innso.api.clientsupport.exception.EntityNotFound;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class InMemoryClientFolderRepository implements ClientFolderRepository {

    private InMemoryDataBase inMemoryDataBase;
    private MessageRepository messageRepository;

    public InMemoryClientFolderRepository(InMemoryDataBase inMemoryDataBase, MessageRepository messageRepository) {
        this.inMemoryDataBase = inMemoryDataBase;
        this.messageRepository = messageRepository;
    }

    @Override
    public String attachMessageToNewClientFolder(String idMessage, ClientFolder newClientFolder) {
        final Message message = messageRepository.findMessageById(idMessage);
        newClientFolder.getMessages().add(message);
        newClientFolder.setId(UUID.randomUUID().toString());
        inMemoryDataBase.getClientFolders().add(newClientFolder);
        return newClientFolder.getId();
    }

    @Override
    public String createClientFolder(ClientFolder clientFolder) {
        clientFolder.setId(UUID.randomUUID().toString());
        clientFolder.getMessages()
                .forEach(msg -> {
                    msg.setId(UUID.randomUUID().toString());
                    inMemoryDataBase.getMessages().add(msg);
                });
        inMemoryDataBase.getClientFolders().add(clientFolder);
        return clientFolder.getId();
    }

    @Override
    public void addMessageToExistingFolder(Message message, String clientFolderId) {
        ClientFolder clientfolderFound = findClientFolderById(clientFolderId);
        messageRepository.createMessage(message);
        clientfolderFound.getMessages().add(message);
    }

    @Override
    public void updateClientFolder(String clientFolderId, ClientFolder clientFolder) {
        ClientFolder clientFolderFound = findClientFolderById(clientFolderId);

        if(!clientFolder.getReference().equals(clientFolderFound.getReference())) {
            clientFolderFound.setReference(clientFolder.getReference());
        }
    }

    @Override
    public List<ClientFolder> fetchClientFolders() {
        return inMemoryDataBase.getClientFolders();
    }

    @Override
    public ClientFolder findClientFolderById(String clientFolderId) {
        return inMemoryDataBase.getClientFolders().stream()
                .filter(cf -> cf.getId().equals(clientFolderId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFound("Client folder not found for client ID:" + clientFolderId));
    }


}
