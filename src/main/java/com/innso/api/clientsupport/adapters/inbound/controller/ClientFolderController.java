package com.innso.api.clientsupport.adapters.inbound.controller;

import com.innso.api.clientsupport.adapters.inbound.dto.ClientFolderDto;
import com.innso.api.clientsupport.domain.ClientFolder;
import com.innso.api.clientsupport.service.ClientFolderService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
public class ClientFolderController {

    private ModelMapper modelMapper;
    private ClientFolderService clientFolderService;

    public ClientFolderController(ModelMapper modelMapper, ClientFolderService clientFolderService) {
        this.modelMapper = modelMapper;
        this.clientFolderService = clientFolderService;
    }

    @PostMapping("/clientfolder/new")
    public ResponseEntity<URI> createClientFolder(@RequestBody ClientFolderDto clientFolderDto) {
        final ClientFolder clientFolder = modelMapper.map(clientFolderDto, ClientFolder.class);
        final String clientFolderId = clientFolderService.createClientFolder(clientFolder);
        return ResponseEntity.ok(URI.create("/clientfolder/" + clientFolderId));
    }

    @GetMapping("/clientfolder/{clientFolderId}")
    public ResponseEntity<ClientFolderDto> findClientFolder(@PathVariable String clientFolderId) {
        final ClientFolder clientFolder = clientFolderService.findClientFolder(clientFolderId);
        final ClientFolderDto clientFolderDto = modelMapper.map(clientFolder, ClientFolderDto.class);
        return ResponseEntity.ok(clientFolderDto);
    }

    @GetMapping("/clientfolders")
    public ResponseEntity<Set<ClientFolderDto>> fetchClientFolders(){
        final Set<ClientFolderDto> clientFolders = clientFolderService.fetchClientFolders().stream()
                .map(cf -> modelMapper.map(cf, ClientFolderDto.class))
                .collect(Collectors.toSet());

        return ResponseEntity.ok(clientFolders);
    }

    @PostMapping("/clientfolder/with/message/{messageId}")
    public ResponseEntity<URI> attachMessageToClientFolder(@PathVariable String messageId, @RequestBody ClientFolderDto clientFolderDto) {
        final ClientFolder clientFolder = modelMapper.map(clientFolderDto, ClientFolder.class);
        final String clientFolderId = clientFolderService.attachMessageToClientFolder(messageId, clientFolder);
        return ResponseEntity.ok(URI.create("/clientfolder/" + clientFolderId));
    }

    @PutMapping("/clientfolder/{clientFolderId}/update")
    public ResponseEntity updateClientFolder(@PathVariable String clientFolderId, @RequestBody ClientFolderDto clientFolderDto) {
        final ClientFolder clientFolder = modelMapper.map(clientFolderDto, ClientFolder.class);
        clientFolderService.updateClientFolder(clientFolderId, clientFolder);
        return ResponseEntity.ok().build();
    }


}
