package com.innso.api.clientsupport.adapters.inbound.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Setter
@Getter
public class MessageDto {
    private String id;
    private String date;
    private String authorName;
    private String content;
    private Canal canal;
}

