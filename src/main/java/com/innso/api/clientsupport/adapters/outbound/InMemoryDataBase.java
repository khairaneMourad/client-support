package com.innso.api.clientsupport.adapters.outbound;

import com.innso.api.clientsupport.domain.ClientFolder;
import com.innso.api.clientsupport.domain.Message;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;


public class InMemoryDataBase {
    private List<ClientFolder> clientFolders;
    private List<Message> messages;

    public List<ClientFolder> getClientFolders() {
        if(clientFolders == null)
            clientFolders = new LinkedList<>();
        return clientFolders;
    }

    public List<Message> getMessages() {
        if(messages == null)
            messages = new LinkedList<>();
        return messages;
    }
}
