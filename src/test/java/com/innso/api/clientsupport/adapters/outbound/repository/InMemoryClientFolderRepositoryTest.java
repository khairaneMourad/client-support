package com.innso.api.clientsupport.adapters.outbound.repository;

import com.innso.api.clientsupport.adapters.outbound.InMemoryDataBase;
import com.innso.api.clientsupport.domain.Canal;
import com.innso.api.clientsupport.domain.ClientFolder;
import com.innso.api.clientsupport.domain.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InMemoryClientFolderRepositoryTest {

    public static final String JEREMIE_DURANT = "Jérémie Durant";
    InMemoryDataBase inMemoryDataBase;
    MessageRepository messageRepository;
    ClientFolderRepository inMemoryClientFolderRepository;

    @BeforeEach
    void setUp() {
        inMemoryDataBase = new InMemoryDataBase();
        messageRepository = new InMemoryMessageRepository(inMemoryDataBase);
        inMemoryClientFolderRepository = new InMemoryClientFolderRepository(inMemoryDataBase, messageRepository);
    }

    @Test
    void attachMessageToNewClientFolder() {
        Message message = Message.MessageBuilder.aMessage()
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .canal(Canal.MAIL)
                .date(LocalDateTime.now())
                .build();

        messageRepository.createMessage(message);

        ClientFolder clientFolder = ClientFolder.Builder.aClientFolder()
                .withClientName(JEREMIE_DURANT)
                .build();

        inMemoryClientFolderRepository.attachMessageToNewClientFolder(message.getId(), clientFolder);

        final Message newMessage = inMemoryDataBase.getClientFolders().get(0).getMessages().get(0);
        assertEquals(1, inMemoryDataBase.getMessages().size());
        assertEquals(JEREMIE_DURANT, inMemoryDataBase.getMessages().get(0).getAuthorName());
        assertEquals(1, inMemoryDataBase.getClientFolders().size());
        assertEquals(message.getId(), newMessage.getId());
        assertEquals(message.getContent(), newMessage.getContent());
    }

    @Test
    void addMessageToExistingFolder() {
        ClientFolder clientFolder = ClientFolder.Builder.aClientFolder()
                .withClientName(JEREMIE_DURANT)
                .build();

        inMemoryClientFolderRepository.createClientFolder(clientFolder);

        Message message = Message.MessageBuilder.aMessage()
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .canal(Canal.MAIL)
                .date(LocalDateTime.now())
                .build();

        inMemoryClientFolderRepository.addMessageToExistingFolder(message, clientFolder.getId());
        assertEquals(1, inMemoryDataBase.getClientFolders().size());
        assertEquals(1, inMemoryDataBase.getMessages().size());
        assertEquals(JEREMIE_DURANT, inMemoryDataBase.getClientFolders().get(0).getMessages().get(0).getAuthorName());

    }

    @Test
    void updateClientFolderReference() {
        Message message = Message.MessageBuilder.aMessage()
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .canal(Canal.MAIL)
                .date(LocalDateTime.now())
                .build();

        ClientFolder clientFolder = ClientFolder.Builder.aClientFolder()
                .withClientName(JEREMIE_DURANT)
                .withMessages(List.of(message))
                .build();

        inMemoryClientFolderRepository.createClientFolder(clientFolder);

        ClientFolder newClientFolder = ClientFolder.Builder.aClientFolder()
                .withId(clientFolder.getId())
                .withClientName(JEREMIE_DURANT)
                .withReference("KA-18B6")
                .build();

        inMemoryClientFolderRepository.updateClientFolder(clientFolder.getId(), newClientFolder);

        assertEquals(1, inMemoryDataBase.getClientFolders().size());
        assertEquals(1, inMemoryDataBase.getMessages().size());
        assertEquals("KA-18B6", inMemoryDataBase.getClientFolders().get(0).getReference());
    }

    @Test
    void fetchClientFolders() {
        Message message = Message.MessageBuilder.aMessage()
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .canal(Canal.MAIL)
                .date(LocalDateTime.now())
                .build();

        ClientFolder clientFolder = ClientFolder.Builder.aClientFolder()
                .withClientName(JEREMIE_DURANT)
                .withMessages(List.of(message))
                .build();

        inMemoryClientFolderRepository.createClientFolder(clientFolder);
        final List<ClientFolder> clientFolders = inMemoryClientFolderRepository.fetchClientFolders();
        assertEquals(1, clientFolders.size());
        assertEquals(JEREMIE_DURANT, clientFolders.get(0).getClientName());
    }
}