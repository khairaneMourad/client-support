package com.innso.api.clientsupport.adapters.outbound.repository;

import com.innso.api.clientsupport.adapters.outbound.InMemoryDataBase;
import com.innso.api.clientsupport.domain.Canal;
import com.innso.api.clientsupport.domain.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InMemoryMessageRepositoryTest {

    public static final String JEREMIE_DURANT = "Jérémie Durant";
    private InMemoryDataBase inMemoryDataBase;
    private MessageRepository messageRepository;

    @BeforeEach
    void setUp() {
        inMemoryDataBase = new InMemoryDataBase();
        messageRepository = new InMemoryMessageRepository(inMemoryDataBase);
    }

    @Test
    void updateMessage() {
        Message message = Message.MessageBuilder.aMessage()
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .canal(Canal.MAIL)
                .date(LocalDateTime.now())
                .build();

        messageRepository.createMessage(message);

        final LocalDateTime now = LocalDateTime.now();
        Message newMessage = Message.MessageBuilder.aMessage()
                .id(message.getId())
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau PC")
                .canal(Canal.FACEBOOK)
                .date(now)
                .build();

        messageRepository.updateMessage(newMessage);
        assertEquals(1, inMemoryDataBase.getMessages().size());
        assertEquals(Canal.FACEBOOK, inMemoryDataBase.getMessages().get(0).getCanal());
        assertEquals("Bonjour, j’ai un problème avec mon nouveau PC", inMemoryDataBase.getMessages().get(0).getContent());
        assertEquals(now, inMemoryDataBase.getMessages().get(0).getDate());
    }

    @Test
    void fetchMessagesByAuthor() {

        Message message1 = Message.MessageBuilder.aMessage()
                .authorName(JEREMIE_DURANT)
                .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                .canal(Canal.MAIL)
                .date(LocalDateTime.now())
                .build();

        Message message2 = Message.MessageBuilder.aMessage()
                .authorName("MOURAD KHAIRANE")
                .content("Bonjour, j’ai un problème avec mon nouveau PC")
                .canal(Canal.TWITTER)
                .date(LocalDateTime.now())
                .build();

        inMemoryDataBase.getMessages().add(message1);
        inMemoryDataBase.getMessages().add(message2);

        final List<Message> messages = messageRepository.fetchMessagesByAuthor("MOURAD KHAIRANE");
        assertEquals(1, messages.size());
    }
}